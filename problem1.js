function problem1(data) {

    let errorMessage;
    if(!Array.isArray(data)) {
        errorMessage = "Data is not an array. Please enter the data in an Array";
        return errorMessage;
    }

    if(data.length === 0) {
        errorMessage = "No data present";
        return errorMessage;
    }
    
    let emailAddresses = [];
    for(let index=0; index<data.length; index++) {
        emailAddresses.push(data[index].email)
    }

    return emailAddresses;
}

module.exports = problem1;