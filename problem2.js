function problem2(data, age) {

    let errorMessage;
    if(!Array.isArray(data)) {
        errorMessage = "Data is not an array. Please enter the data in an Array";
        return errorMessage;
    }

    if(data.length === 0) {
        errorMessage = "No data present";
        return errorMessage;
    }
    
    let hobbies = [];
    for(let index=0; index<data.length; index++) {
        if(data[index].age === age) {
            hobbies.push(data[index].hobbies);
        }
    }

    return hobbies;
}

module.exports = problem2;