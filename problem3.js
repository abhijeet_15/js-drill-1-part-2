function problem3(data) {

    let errorMessage;
    if(!Array.isArray(data)) {
        errorMessage = "Data is not an array. Please enter the data in an Array";
        return errorMessage;
    }

    if(data.length === 0) {
        errorMessage = "No data present";
        return errorMessage;
    }
    
    let studentsLivingInAustralia = [];
    for(let index=0; index<data.length; index++) {
        if(data[index].isStudent && data[index].country === "Australia") {
            studentsLivingInAustralia.push(data[index].name);
        }
    }
    return studentsLivingInAustralia;
}

module.exports = problem3;