function problem4(data) {

    let errorMessage;
    if(!Array.isArray(data)) {
        errorMessage = "Data is not an array. Please enter the data in an Array";
        return errorMessage;
    }

    if(data.length === 0) {
        errorMessage = "No data present";
        return errorMessage;
    }
    
    return {name: data[3].name, city: data[3].city}
}

module.exports = problem4;