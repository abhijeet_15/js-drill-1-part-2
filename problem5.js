function problem5(data) {

    let errorMessage;
    if(!Array.isArray(data)) {
        errorMessage = "Data is not an array. Please enter the data in an Array";
        return errorMessage;
    }

    if(data.length === 0) {
        errorMessage = "No data present";
        return errorMessage;
    }
    
    let ages = [];
    for(let index=0; index<data.length; index++) {
        ages.push(data[index].age);
    }
    return ages;
}

module.exports = problem5;