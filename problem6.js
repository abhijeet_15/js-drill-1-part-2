function problem6(data) {

    let errorMessage;
    if(!Array.isArray(data)) {
        errorMessage = "Data is not an array. Please enter the data in an Array";
        return errorMessage;
    }

    if(data.length === 0) {
        errorMessage = "No data present";
        return errorMessage;
    }
    
    let firstHobby = [];
    for(let index=0; index<data.length; index++) {
            firstHobby.push(data[index].hobbies[0]);
    }
    return firstHobby;
}

module.exports = problem6;