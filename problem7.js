function problem7(data) {
    let errorMessage;
    if(!Array.isArray(data)) {
        errorMessage = "Data is not an array. Please enter the data in an Array";
        return errorMessage;
    }

    if(data.length === 0) {
        errorMessage = "No data present";
        return errorMessage;
    }

    let namesAndEmails = [];
    for(let index=0; index<data.length; index++) {
        if(data[index].age === 25) {
            namesAndEmails.push({name: data[index].name, email: data[index].email});
        }
    }
    return namesAndEmails;
}

module.exports = problem7;