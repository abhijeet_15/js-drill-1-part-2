function problem8(data) {
    let errorMessage;
    if(!Array.isArray(data)) {
        errorMessage = "Data is not an array. Please enter the data in an Array";
        return errorMessage;
    }

    if(data.length === 0) {
        errorMessage = "No data present";
        return errorMessage;
    }

    let cityAndCoutryOfIndividuals = [];
    for(let index=0; index<data.length; index++) {
        cityAndCoutryOfIndividuals.push({city: data[index].city, country: data[index].country});
    }
    return cityAndCoutryOfIndividuals;
}

module.exports = problem8;